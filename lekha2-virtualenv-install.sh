

mkvirtualenv lekhaocr -p python3
workon lekhaocr


#installing python and pip3 for installing packages
sudo apt-get install -y python3
sudo apt-get install -y python3-pip
sudo pip3 install --upgrade pip
sudo apt-get install -y python3-tk

#install smc fonts to read the output properly
sudo apt-get install -y fonts-smc
#to allow system gi acess in virtual env
pip3 install vext
pip3 install vext.gi

sudo apt install python3-gi python3-gi-cairo gir1.2-gtk-3.0
