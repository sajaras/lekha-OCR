# Lekha Ocr version 2.0


## Description

Lekha Ocr version 2.0 Converts your malayalam documents and images to editable malayalam text.It's designed to be easy,fast and simple to use.it has add-on features like scanning,croping,rotating and skew correction of images which help you to do things easy.

In other words, let the machine do most of the malayalam typing work for you.


## Screenshots

### Main Window


![ScreenShot](screenshots/screenshot_mainwindow.png)

### Main Window after convertion

![ScreenShot](screenshots/screenshot_output.png)

### Settings Window

![ScreenShot](screenshots/screenshot_settings.png)

### Layout Analyzed Image

![ScreenShot](screenshots/screenshot_layout.png)

### Crop

![ScreenShot](screenshots/screenshot_crop.png)



## Main features

* Scan
* Import image from disk
* Image layout analysis and feature to correct the result : draw new box by selecting and then press enter to confirm  ,delete the incorrect layout analysed box by clicking on respective box.
* Malayalam ocr (lekha_ocrv2)
* [Image Skew correction](https://github.com/kakul/Alyn)
* Rotate image
* Crop image : crop out unwanted parts of imported/scanned image using crop functionality.
* Set Workspace Directory
* Save Output malayalam text to text file.

## Requirement
* Python3 : application runs only on python3,Doest support on python2
* Opencv version >= 3.0
* Gtk version >=3.20






## Installation : Debian/Ubuntu ( Normal Installation)

* git clone https://gitlab.com/space-kerala/LEKHA_OCR_VER2.0.git
  (or Download zip and extract)
* Navigate to the cloned or extracted directory
* sudo apt-get update
* sudo bash lekha2-install.sh
* installation done  
* Open the application by searching lekha ocr ver 2 in the dash
* Set workspace directory under edit-> preference before any scan operation.

## To Uninstall
* sudo bash uninstall.sh



## Virtual Environment Installation : Debian/Ubuntu (Recommended for machines in developer Environment)

* git clone https://gitlab.com/space-kerala/LEKHA_OCR_VER2.0.git
  (or Download zip and extract)
* Navigate to the cloned or extracted directory
* sudo apt-get update
* #setting virtual environment if does't have any.Rename the virtualdirectory with your required virtualenvironment directory name.
 * pip3 install virtualenv virtualenvwrapper
 * echo -e "\n# virtualenv and virtualenvwrapper" >>
 * ~/.bashrc
 * echo "export WORKON_HOME=$HOME/virtualdirectory" >>
 * ~/.bashrc
 * echo "source /usr/local/bin/virtualenvwrapper.sh" >>
 * ~/.bashrc
 * source ~/.bashrc
 * sudo bash lekha2-virtualenv-install.sh

* #installing dependencies and application from setup.py to virtual environment
* /home/username/virtualdirectory/lekhaocr/bin/python3 setup.py install

* #To have a dash board launcher for the application
* sudo nano lekhaocrvirtualenv.desktop
* edit the lekhaocrvirtualenv.desktop file with your corresponding username and virtualdirectoryname and python3 version index.
* cp lekhaocrvirtualenv.desktop /usr/share/applications/

* installation done  
* Open the application by searching lekha ocr ver 2 in the dash
* Set workspace directory under edit-> preference before any scan operation.




## Details

It mainly uses:

* [Pyinsane2](https://pypi.python.org/pypi/pyinsane2): To scan the pages
* [OPENCV](https://github.com/opencv/opencv): for image manipulation and calculations
* [Lekha_ocrv2](https://github.com/space-kerala/LEKHA_OCR_VER2.0/tree/master/lekha_ocr)(OCR)
* [GTK+](http://www.gtk.org/): For the user interface
* [PYGOBJECT](https://pygobject.readthedocs.io/): python binding for Gtk+
* [Matplotlib](https://matplotlib.org/): Selection and for layout analyzed output correction features
* [Pillow](https://pypi.python.org/pypi/Pillow/)


## Creators
  Team under Arun M    
* [Sachin Gracious](https://github.com/sachingracious)
* [Sajaras k](https://github.com/sajaras)
* [Yadhukrishnan K](https://github.com/yadu17)

## Contributors
* Arun M helped in project management and technical assistance.
* Ambily Sreekumar,contributed in building data set for training.
* [Jithin thankachan](https://github.com/jithin-space):contributed in training tool and helped in documentation.
* [Arun Joseph](https://github.com/arunjoseph0):contributed most of the engine initial  developments of [Lekha ocr 1.0](https://gitlab.com/space-kerala/lekha-OCR)
* Balagopal Unnikrishnan : Contributed in preparing XML label for training and helped in documentation of[Lekha ocr 1.0](https://gitlab.com/space-kerala/lekha-OCR)
* Rijoy V : Contributed in initial research of [Lekha OCR ver 1.0](https://gitlab.com/space-kerala/lekha-OCR)

## Developers

This project is a joint initiative of [SPACE-KERALA](http://www.space-kerala.org/) & [ICFOSS](https://icfoss.in)


## Licence

GPLv3 only. See License.md


## Development

All the information can be found on [ICFOSS-Gitlab](https://gitlab.com/icfoss/lekha-OCR)
