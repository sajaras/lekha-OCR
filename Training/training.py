
# coding: utf-8

# In[1]:


import cv2
import json
import codecs
import os
import features
import pandas as pd
from math import sqrt,atan2,floor
from sklearn import svm
from sklearn.externals import joblib
from sklearn.preprocessing import normalize,Normalizer


# In[2]:


responses=[]
sample=[]


# In[3]:


print('Reading configurations')
conf = json.load(open('conf.json', 'r'))
database = conf['database']
savepath = conf['data_save_path']
clf_path=conf['clf_path']
min_samples_per_label = 20


# In[4]:


alist_filter = ['.png','.jpg','.bmp','.gif','.tiff']
for dirName, subdirList, fileList in (os.walk(database)):
    file_path = os.path.join(dirName,'utf8')
    if len(fileList)>20:
        fp=codecs.open(file_path,"r",encoding='utf-8')
        label_u=(fp.readline().strip()).encode('UTF-8')
        print label_u ,len(fileList)
        for fname in fileList:
            if os.path.splitext(fname)[1] in alist_filter:
                responses.append(label_u)
                file_path = os.path.join(dirName, fname)
                img=cv2.imread(file_path.encode('utf-8'),0)
                im = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY_INV,243,50)
                sample.append(features.HOG(im))
    


# In[7]:



X_sample_n = Normalizer().fit(sample)
X_sample=X_sample_n.transform(sample)

df = pd.DataFrame(sample)
df['target'] = responses

clf=svm.SVC(decision_function_shape='ovo',C="", gamma="")
clf.set_params(kernel='rbf').fit(X_sample,responses)
joblib.dump(clf,clf_path)
print('Saving to file')
df = df.iloc[np.random.permutation(len(df))]
df.to_pickle(savepath)

